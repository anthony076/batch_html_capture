# batch-html-capture
[batch-html-capture] 添加命令列執行功能

## 功能
解析命令列參數 -> 擷取所有分頁的網址 或 讀取分頁網址 -> 取得主題名稱，並建立資料夾 -> 進入分頁網址，取得所有文章連結 -> 進入文章連結 -> 元素截圖 -> 存檔

支援的網站: ithelp、juejin、jianshu、zhihu、cnblogs

## 技術線
- cheerio
- puppeteer
- request
- lodash
- yargs

## TODO
- 建立 GUI (with Electron)
- 提供 客製化 css-selector 的功能
- 在 default-setting 終究提供網站判斷的功能，例如 paginated:true，scroll:true
- 若頁面中有 github 連結，則下載 repo
- 提供 config
- 添加 continues mode，cmd 進入 listen-loop，使用者輸入網址後，就全螢幕截圖，不需要配置 css
- 加入雲服務，需先確認 puppeteer 是否能在雲平台上執行

## 測試
- it邦單篇文章，bhc https://ithelp.ithome.com.tw/articles/10184853
- it邦鐵人賽文章列表，bhc -l https://ithelp.ithome.com.tw/users/20103361/ironman/1043

- 掘金單篇文章，bhc https://juejin.im/post/5c6805146fb9a049fa104d18
- 掘金收藏集，bhc -l https://juejin.im/collection/5c88b02ae51d453f6715b38e
- 掘金個人專欄，bhc -l https://juejin.im/user/5a6c041ef265da3e3e34019a/posts

(注意，簡書需要開啟 UI)
- 簡書單篇文章，bhc https://www.jianshu.com/p/698b44eaa900
- 簡書用戶文章列表，bhc -l https://www.jianshu.com/u/112c6a5df44d
- 簡書文集列表，bhc -l https://www.jianshu.com/nb/10098148
- 簡書專題列表，bhc -l https://www.jianshu.com/c/69b548699816

(注意，之乎網站需要捲動，因此截圖的時間需要更長)
- 之乎單篇文章，bhc https://zhuanlan.zhihu.com/p/29668000
(link not available)
- 之乎用戶文章列表，bhc -l https://www.zhihu.com/people/.........
- 之乎用戶專欄列表，bhc -l https://zhuanlan.zhihu.com/c_1090909802070233088
- 之乎公共話題列表，bhc -l https://www.zhihu.com/topic/20688196/hot

- cnblogs
- 博客園書籤列表，bhc -l https://www.cnblogs.com/champagne/tag/Google%20Chrome%E6%89%A9%E5%B1%95/

## Changelog
v20190809:<br>
- 添加 博客園書籤列表截圖

v20190809:<br>
- 代碼重構 + 代碼分拆
- 添加 之乎網站公共話題截圖

v20190807:<br>
- 添加 知乎網站 的支援
    - 支援知乎網站的 1_單篇文章截圖 2_用戶文章列表截圖 3_公共話題截圖(不支援話題列表網頁中，屬於回答問題類型的文章) 
    4_用戶專欄截圖

- 在列表網頁中，支援擷取連結前，先進行捲動，以獲取完整的文章連結

v20190806:<br>
- 添加 簡書網站 的支援
    - 支援掘金網站的 1_單篇文章截圖 2_用戶文章列表截圖 3_文集截圖 4_專題截圖
    - 簡書的捲動速度設置為 700ms，避免加載圖片不完全

- 將截圖的處理函數改為 scrollToBottom() 和 noScrolDown_handler() 兩種，並將輸入參數改為 object 的形式，方便擴充輸入參數

v20190804:<br>
- 添加 掘金網站 的支援
    - 支援掘金網站的 1_單篇文章截圖 2_個人專欄截圖 3_收藏集截圖
    - 掘金網站的響應速度慢，需要將 timeout 設置為 0
    - 掘金網站是透過捲動異步加載圖片，截圖前需要先捲動頁面，確保圖片已經正確被加載
- 將截圖的處理改為 handle()，handle(siteName) 根據提供的 siteName 返回對應的處理函數
- 將命令列參數的 -u 改名為 -l，代表該網址是文章列表的網址

v20190123:<br>
- 把 getUrls() 和 linksByUrl() 共用的部分拆分成 contentParser()
- contentParser() 可以根據不同的輸入參數，自動選擇從 file 或從 url 取得截圖文章的連結
    - contentParser() 用於 url 時，需要提供 url、selector 和 selector 的處理函數
    - contentParser() 用於 file 時，需要提供檔案路徑
- 添加 getTitle()，從主列表網頁取得主題名稱，依照主題名稱建立截圖存放的資料夾
- 添加命令列執行的功能

v20190122:<br>
- add getUrls()，只需要給主列表網頁的網址，getUrls() 會取得該網址中的分頁數後，自動產生 urls
- add siteSelector()，根據主列表網頁的網址，siteSelector()會自動返回該網站中所有相關的 css-selector
- filterString()，用來過濾截圖檔名中的特殊字，避免存檔錯誤
- 將截圖的檔名改成以文章標題命名

## Bugs
- 如果 url 輸入為 https://ithelp.ithome.com.tw/users/20111656/ironman/1676?page=2 <br>
urls 會變成 https://ithelp.ithome.com.tw/users/20111656/ironman/1676?page=2?page1 導致 永遠在第一頁
