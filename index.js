/*
Work-Flow
    #1，add css selector in 
    #2，modify getSiteName() in
    #3, modify Selector() in 
    #4，modify handle() in 
    #5，verify result
*/
const fs = require('fs');

const { getParaType } = require("./lib/functions")
const { Linker } = require("./lib/linker")
const { Handler } = require("./lib/capturer")

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "1";

// ========================
// 取得命令列參數
// ========================
const argv = require('yargs')
    .usage('Usage: bhc [Options] ')
    //.default("f", "./links.txt")     // 建立參數預設值
    .alias("l", "list")              // 定義具名參數
    .alias("f", "file")
    // 不添加此限制，因為不是每次都需要無具名參
    // 限制無名參數的最短長度，可以超過，但不能低於
    //.demandCommand(1)  
    .example("bhc https://ithelp.ithome.com.tw/articles/XXXX")
    .example("bhc -l https://ithelp.ithome.com.tw/ironman/articles/XXXX")
    .example("bhc -f path/to/links.txt")
    .nargs("l", 1)  // 限定具名參數需要提供的參數值，例如 -l 需要一個參數值
    .nargs("f", 1)
    .describe("l", "url that contains article list")
    .describe("f", "file path of links.txt")    // 參數的說明，--help 的時候會顯示
    .argv;

// ========================
// Main
// ========================
(async () => {
    // ==== Step1. 取得使用者輸入的命令列類型和值 ====
    let { type, para } = getParaType(argv);

    // ==== Step2. 取得所有文章連結 ====
    let { links, site, folderName } = await Linker({ type, para })

    // ==== Step3. create folder ====
    if (!fs.existsSync(folderName)) {
        fs.mkdirSync(folderName);
    }

    // ==== Step4. start screenshot ====
    console.log(site)
    let siteName = site.siteName
    let selector = site.selector

    const handler = Handler(siteName)

    if (siteName.includes("jianshu") | siteName.includes("zhihu")) {
        await handler({ links, selector, folderName, delay: 1000 })
    } else {
        await handler({ links, selector, folderName })
    }

    await browser.close()
})()
