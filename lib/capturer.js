
const fs = require('fs');

const { cwd } = require("./functions.js");
const { filterString, scrollToBottom } = require("./linker")

var CWD = cwd()

async function outputRename(css_title, folderName, outFileName) {
    let outFile = outFileName

    // 重新命名檔名
    // 因為 png 檔有可能因為特殊字造成無法寫入，讓 puppeteer 先寫入英文檔名
    // 後面利用 fs 更改成中文檔名，若更改失敗，至少還能保有英文檔名
    var result = await page.$eval(css_title, el => el.innerText).catch(() => console.log("title not found"))
    result = result || "screen"

    var title = exportFunctions.filterString(result)

    let newName = `${CWD}\\${folderName}\\${title}.png`

    fs.rename(outFile, newName, function (err) {
        if (err) console.log('ERROR: ' + err);
    });
}

async function handle_noScrolDown(linker) {
    var count = 1
    let { links, selector, folderName } = linker

    for (var link of links) {
        console.log(`open : ${link}`)

        // 跳轉頁面
        // waitUntil: 'load'，等待 DOM樹建構 + 外部資源都加載完畢
        // waitUntil: 'domcontentloaded'，等待 DOM樹建構
        //await page.goto(link, { waitUntil: 'load', timeout: 0 })
        await page.goto(link, { waitUntil: "networkidle2" })
        await page.waitForSelector(selector.capture_el)

        // 元素截圖
        const outFileName = `${CWD}\\${folderName}\\screen-${count}.png`

        let node = await page.$(selector.capture_el)
        await node.screenshot({ path: outFileName })

        console.log(`${outFileName} output ...`)

        await outputRename(selector.article_el, folderName, outFileName)

        count += 1
    }
}

async function handle_scrolDown(linker) {
    var count = 1
    let { links, selector, folderName, delay } = linker

    for (var link of links) {
        console.log(`open : ${link}`)

        // { waitUntil: 'load', timeout: 0 }，避免 timeout
        //await page.goto(link, { waitUntil: 'load', timeout: 0 })
        //await page.goto(link, { timeout: 0 })
        await page.goto(link, { waitUntil: "networkidle2" })
        //await page.waitFor(1000)
        await page.waitForSelector(selector.capture_el)

        // 捲動頁面，簡書採用懶加載圖片 (需捲動才會發出圖片的異步請求)
        // 注意，delay < 500，速度太慢可能會造成圖片加載不完全
        await page.evaluate(scrollToBottom, delay);

        // 元素截圖
        const outFileName = `${CWD}\\${folderName}\\screen-${count}.png`

        // 尋找要被截圖的 node
        let node = await page.$(selector.capture_el)
        await node.screenshot({ path: outFileName })

        console.log(`${outFileName} output ...`)

        await outputRename(selector.article_el, folderName, outFileName)

        count += 1
    }
}

// Handler 代理器
function Handler(siteName) {
    if (siteName.includes("ithelp") | siteName.includes("jb51") | siteName.includes("cnblogs") | siteName.includes("temp")) {
        // TODO:
        // let targets = ["ithelp", "jb51", "cnblogs","temp"]
        // targets.some( el => siteName.includes(el))

        // 不需捲動加載圖片
        return handle_noScrolDown

    } else if (siteName.includes("juejin") | siteName.includes("jianshu") | siteName.includes("zhihu")) {
        // 需捲動加載圖片
        return handle_scrolDown

    } else {
        throw new Error(`Unknown site handler : ${siteName} is not supported.`)
    }
}

const exportFunctions = { Handler, handle_noScrolDown, handle_scrolDown, outputRename, filterString }

module.exports = exportFunctions