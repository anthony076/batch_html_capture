
module.exports = {
    chrome: {
        headless: true,
        devtools: false,
        executablePath: "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe"
    },
    links: {
        have_paginate_url: ["ithelp", "zhihu_people"],
        have_scroll_url: ["zhihu_zhuanlan", "zhihu_topic"],
        noPaginate_noScroll: ["jb51"]
    },

    capture: {

    },

    re_pattern: {
        //zhihu_p: /zhuanlan.*p/,
        zhihu_p: /zhuanlan.zhihu.com\/p\/.*/,
        //zhihu_zhuanlan: /zhihu(?!.*p)/,
        zhihu_zhuanlan: /zhuanlan.zhihu.com\/(?!.*\/)/,
        zhihu_people: /zhihu.*people/,
        zhihu_topic: /zhihu.*topic/
    },

    support_site: ["ithelp", "juejin", "jianshu", "zhihu", "jb51", "cnblogs"]
};

