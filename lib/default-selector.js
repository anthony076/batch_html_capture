exports.ithelp = {
    name: "ithelp",
    // ==== For article-list url ====
    // 系列文名稱
    title_el: "body > div.container.index-top > div > div > div.board.leftside.profile-main > div.ir-profile-content > div.ir-profile-series > h3",
    // 列表中的文章連結
    link_el: ".qa-list__title-link",
    // 列表分頁
    //paginate_el: "body > div.container.index-top > div > div > div.board.leftside.profile-main > div.profile-pagination > ul",
    paginate_el: ".profile-pagination > ul > li",
    // ==== For article url ====
    // 單篇文章標題
    article_el: ".qa-header__title",
    // 欲擷圖的元素
    capture_el: "body > div.container.index-top > div > div > div.leftside > div.qa-panel.clearfix",
}

// 用於掘金-單篇文章
exports.juejin_post = {
    name: "juejin_post",
    title_el: null,
    link_el: null,
    paginate_el: null,
    article_el: ".article-title",
    capture_el: "#juejin > div.view-container > main",
}

// 用於掘金-收藏集
// 網址範例，https://juejin.im/collection/5c88b02ae51d453f6715b38e
exports.juejin_collection = {
    name: "juejin_collection",
    title_el: "main > div.collection-view > header > h1",
    link_el: ".title-row > a",
    paginate_el: null,
    article_el: ".article-title",
    capture_el: "#juejin > div.view-container > main",
}

// 用於掘金-專欄
// 網址範例，https://juejin.im/user/5a6c041ef265da3e3e34019a/posts
exports.juejin_user = {
    name: "juejin_user",
    title_el: ".top > .username",
    link_el: ".abstract-row > a.title",
    paginate_el: null,
    article_el: ".article-title",
    capture_el: "#juejin > div.view-container > main",
}

// 單篇文章，需捲動加載，https://www.jianshu.com/p/698b44eaa900
// 用戶文章列表，小量文章，無分頁，需捲動加載，https://www.jianshu.com/u/112c6a5df44d
// 專題，網站分類，大量文章，無分頁，需捲動加載，https://www.jianshu.com/c/69b548699816
// 文集，個人分類，小量文章，無分頁，需捲動加載，https://www.jianshu.com/nb/10098148
exports.jianshu = {
    name: "jianshu",
    title_el: ".main-top > .title > a",
    link_el: "li > .content > .title",
    paginate_el: null,
    //article_el: ".note > .post > .article > h1",
    article_el: "body > div > div > div:nth-child(1) > div:nth-child(1) > section > h1",
    capture_el: "body > div > div > div:nth-child(1) > div:nth-child(1) > section",
}

// 單篇文章，需捲動加載，https://zhuanlan.zhihu.com/p/29668000
// 用戶文章列表，小量文章，有分頁，有捲動加載，但不需要，https://www.zhihu.com/people/wxyyxc1992/posts
// 專欄，個人分類，小量文章，無分頁，需捲動加載，https://zhuanlan.zhihu.com/wxyyxc1992
// 話題，網站分類，大量文章，無分頁，需捲動加載，https://www.zhihu.com/topic/20105060/hot

// 用於之乎-單篇文章
// 網址範例，https://zhuanlan.zhihu.com/p/29668000
exports.zhihu_p = {
    name: "zhihu_p",
    title_el: null,
    link_el: null,
    paginate_el: null,
    article_el: "main > div > article > header > h1.Post-Title",
    capture_el: "main > div > article",
}

// 用於之乎-用戶文章列表
// 網址範例，https://www.zhihu.com/people/wxyyxc1992/posts
exports.zhihu_people = {
    name: "zhihu_people",
    title_el: "h1 > .ProfileHeader-name",
    link_el: "div > h2.ContentItem-title > a",
    paginate_el: "div.Pagination > button.Button.PaginationButton.Button--plain",
    article_el: "main > div > article > header > h1.Post-Title",
    capture_el: "main > div > article",
}

// 用於之乎-個人專欄列表
// 網址範例，https://zhuanlan.zhihu.com/wxyyxc1992
exports.zhihu_zhuanlan = {
    name: "zhihu_zhuanlan",
    title_el: "h1.ColumnHeader-Title",
    link_el: "li > a > h3.ArticleItem-Title",
    paginate_el: null,
    article_el: "main > div > article > header > h1.Post-Title",
    capture_el: "main > div > article",
}

// 用於之乎-公共話題列表
// 網址範例，https://www.zhihu.com/topic/20105060/hot

exports.zhihu_topic = {
    name: "zhihu_topic",
    title_el: "div > h1.TopicCard-titleText",
    // 只抓取話題列表中的文章 (非問題)
    link_el: "h2.ContentItem-title > a",
    // 抓取話題中的文章和已回答的問題
    //link_el: "h2.ContentItem-title > a, h2.ContentItem-title > div > a",
    paginate_el: null,
    article_el: "main > div > article > header > h1.Post-Title",
    capture_el: "main > div > article",
}

exports.jb51 = {
    // 單篇文章，不需要加載，https://www.jb51.net/article/163698.htm
    // 文章列表1，不需要加載，無分頁，https://www.jb51.net/Special/663.htm
    // 文章列表2，不需要加載，有分頁，https://www.jb51.net/list/list_97_1.htm
    name: "jb51",
    title_el: null,
    link_el: null,
    paginate_el: null,
    article_el: ".title > h1.YaHei",
    capture_el: "#content",
}

exports.cnblogs = {
    name: "cnblogs",
    title_el: "#Header1_HeaderTitle",
    link_el: "div.postTitl2 > a",
    paginate_el: null,
    article_el: "#cb_post_title_url",
    capture_el: "#topics > div.post",
}

exports.temp = {
    name: "temp",
    title_el: null,
    link_el: null,
    paginate_el: null,
    article_el: "body > div.wrapper > div > div > div.title > h2 > a",
    capture_el: "body > div.wrapper > div > div",
}


