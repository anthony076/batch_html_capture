
const path = require('path');

// 取得使用者輸入命令的類型
function getParaType(argv) {
    const file = argv.file
    const list_url = argv.list
    const single = argv._[0]

    if (list_url) {
        return { type: "list_url", para: list_url }
    } else if (file) {
        return { type: "file", para: file }
    } else if (single) {
        return { type: "single", para: single }
    } else {
        throw new Error("command-line parameter not available")
    }
}

// 取得當前路徑
function cwd() {
    return path.dirname(__dirname)
}

module.exports = { getParaType, cwd }