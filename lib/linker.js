const fs = require('fs');
const _ = require('lodash');
const request = require('request');
const cheerio = require('cheerio');
const puppeteer = require('puppeteer');

const Site = require("./site")
const config = require("./config")

let baseUrl;

// 過濾標題中的字串
function filterString(title) {
    return title.trim().replace("?", "").replace(": ", "_").replace(":", "_").replace(" : ", "_").replace("/", "_").replace("->", "_").replace("|", "_");
}

// 捲動頁面
function scrollToBottom(delay) {
    // 計算捲動的移動距離，should be less than or equal to window.innerHeight
    let n = window.innerHeight
    let bit = n.toString()
    let distance = n - (n % Math.pow(10, bit.length - 1))

    return new Promise(resolve => {
        const timer = setInterval(() => {
            document.scrollingElement.scrollBy(0, distance);
            if (document.scrollingElement.scrollTop + window.innerHeight >= document.scrollingElement.scrollHeight) {
                clearInterval(timer);
                resolve();
            }
        }, delay);
    });
}

// 靜態解析網頁，不需要動態操作
function staticParser({ url, selector, fn }) {
    return new Promise(function (resolve, reject) {
        request(url, async (err, res, body) => {
            if (err) { reject("[Error] request error ...") }
            else {
                const $ = cheerio.load(body)
                var result = await fn($, selector).catch(e => {
                    reject("[Error] execute function error ...")
                })

                resolve(result)
            }
        })
    })
}

// 取得列表名稱，當作資料夾名稱
function getTitle($, selector) {
    return new Promise(function (resolve, reject) {
        let title = filterString($(selector).text())
        if (title) {
            resolve(title)
        } else {
            reject("[Error] Cant find title ...")
        }
    })
}

// 取得分頁數
// get paginate urls from given url，work for below paginate-type
// For paginate-type: 上一頁 1 2 3 4 5 下一頁
// For paginate-type: 1 2 3 4 ... 11 下一頁
function getPaginateNumber($, selector) {
    return new Promise(function (resolve, reject) {
        // 注意，selector 需回傳 NodeList
        let number = $(selector).eq(-2).text()

        if (number) {
            resolve(number)
        } else {
            reject("[Error] Cant get pageNumber ...")
        }
    })
}

// 取得`列表分頁網址`中的所有連結
function getLinksByUrl($, selector) {
    return new Promise(function (resolve, reject) {
        let links = []

        $(selector).each(function (i, el) {
            link = el.attribs["href"].trim()

            // 避免部分網站的連結，使用相對路徑，而沒有主網域的域名，例如
            // 使用相對路徑，"/post/5c6805146fb9a049fa104d18"
            // 正確網址，https://juejin.im/post/5c6805146fb9a049fa104d18
            if (!link.includes("http")) {
                link = baseUrl + link
            }

            links.push(link)
        })

        if (links.length > 0) {
            resolve(links)
        } else {
            reject("[Error] cant get any link in page ...")
        }

    })

}

function paginateListUrl() {

}

function scrollListUrl() {

}

function simpleListUrl() { }

function _getFolderName(site) {
    let url = site.url
    let selector = site.selector.title_el

    return new Promise(function (resolve, reject) {
        request(url, async (err, res, body) => {
            if (err) { reject(err) }
            else {
                const $ = cheerio.load(body)
                var result = await getTitle($, selector)
                resolve(result)
            }
        })
    })
}

// 從列表網址中取得所有文章連結，分以下三種狀況
// 狀況一，列表中使用分頁，通常不需要捲動
// 狀況二，列表中不使用分頁，但需要瀏覽器捲動才能加載完整列表
// 狀況三，無分頁且不需要捲動
async function _getLinksFromList(site) {
    let links = [];

    let list_url = site.url
    let selector = site.selector
    let siteName = site.siteName

    const paginate_url = config.links.have_paginate_url
    const scroll_url = config.links.have_scroll_url

    // TODO: 直接在 siteObj 中添加 scroll = true 的屬性，會喪失客戶自定義的靈活性?

    // ==== 具有分頁的列表網址且不需要捲動 ====
    if (paginate_url.some((el) => siteName.includes(el))) {
        let page_urls = [];

        // 取得分頁數
        let pageNumber = await staticParser({ url: list_url, selector: selector.paginate_el, fn: getPaginateNumber })

        // 產生列表分頁的網址
        _.range(pageNumber).forEach(el => {
            page_urls.push(list_url + `?page=${el + 1}`)
        });

        // 從列表分頁的網址中，取得所有單篇文章的連結
        // get article links from each paginate_url
        for (page_url of page_urls) {
            let result = await staticParser({ url: page_url, selector: selector.link_el, fn: getLinksByUrl })
            links.push(...result)
        }

        // ==== 需要捲動的列表網址 ====
    } else if (scroll_url.some((el) => siteName.includes(el))) {

        // ==== 不具有分頁的列表網址但需要捲動才能完整取得列表 ====
        //if (/zhihu(?!.*p)/.test(list_url)) {
        if (config.re_pattern.zhihu_zhuanlan.test(list_url)) {
            // 專門用於之乎用戶專欄列表，ex: https://zhuanlan.zhihu.com/c_1090909802070233088
            // 因為之乎專欄列表在瀏覽器中會發生 403 的錯誤，因此改由手動發送 fetch 的方式取得 links
            // TODO: 之乎專欄 每次 fetch 只會回傳 10 筆資料，需要捲動才會回傳完整資料，
            //       因此，直接將 page.on() 取消，直接發出 fetch，並且將 url 中的 limit 設置為 100
            //       fetch 範例，https://zhuanlan.zhihu.com/api/columns/${作者id}/articles?&limit=${文章總數}&offset=${起始位置}

            let userName = list_url.split("/")
            userName = userName[userName.length - 1]
            api = `https://zhuanlan.zhihu.com/api/columns/${userName}/articles?&limit=100&offset=0`

            request(api, async (err, res, body) => {
                json = JSON.parse(body)

                for (post of json.data) {
                    links.push(post.url)
                }
            })

            // 取得 request 回應的緩衝時間，
            // 若沒有緩衝時間，_getLinksFromLis() 會提前結束，造成 links = []
            await page.waitFor(500)

        } else {
            // 狀況2-2，列表中不使用分頁，但需要瀏覽器捲動才能加載完整列表
            await page.goto(list_url)
            await page.waitForSelector(selector.link_el)

            // 捲動頁面
            await page.evaluate(scrollToBottom, 1000);

            // 在瀏覽器中取得連結
            links = await page.evaluate(() => {
                let result = []
                els = Array.from(document.querySelectorAll("h2.ContentItem-title > a"))
                els.forEach(el => {
                    result.push(el.href)
                })
                return result
            });
        }

        // ==== 沒有分頁且不需要捲動的列表網址 ====
    } else {
        let result = await staticParser({ url: list_url, selector: selector.link_el, fn: getLinksByUrl })
        links.push(...result)
    }

    if (links.length == 0) {
        throw new Error("[Error] Cant find any url in provided list_url")
    }

    return links
}

// 從檔案中取得所有文章連結
function _getLinksFromFile(file) {
    return new Promise(function (resolve, reject) {
        fs.readFile(file, 'utf-8', (err, data) => {
            if (!data.includes("http")) throw new Error("Not valid address in links.txt");

            if (err) { reject(err) } else { resolve(data.split("\r\n")) }
        })
    })
}

// 將單一文章的網址，轉為文章連結的列表
function _getLinksFromSingle(single) {
    return new Promise(function (resolve, reject) {
        resolve([single])
    })
}

// 取得截圖的前置條件，links, site, folderName
async function Linker({ type, para }) {
    let opt;
    let folderName

    // 初始化 puppeteer
    global.browser = await puppeteer.launch(config.chrome)
    global.page = await browser.newPage()

    // define method hook
    let method = {
        "list_url": _getLinksFromList,
        "file": _getLinksFromFile,
        "single": _getLinksFromSingle,
    }

    // get method option and folderName
    if (type == "list_url") {
        //console.log(para)
        opt = Site(para)
        folderName = await _getFolderName(opt)
        baseUrl = opt.baseUrl
    } else if (type == "file" | type == "single") {
        opt = para
        folderName = "temp"
    }

    //execute method to get links
    let links = await method[type](opt)
    console.log(`links: ${links}`)

    // 將 Site 切換到 links 中的網址
    let site = Site(links[0])

    return { links, site, folderName }
}

module.exports = { Linker, filterString, staticParser, scrollToBottom, getTitle, getPaginateNumber, getLinksByUrl }