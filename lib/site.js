
const css = require("./default-selector")
const assert = require('assert')
const config = require("./config")

function Site(url) {
    let selector;

    function getSiteName(url) {
        assert(url != undefined, "url not provide")

        // "https://ithelp.ithome.com.tw/users/20107244/ironman/1726"
        // [ 'https', 'ithelp', 'ithome', 'com', 'tw', 'users', '20107244', 'ironman', '1726']
        let siteName;
        let baseUrl;

        const u = new URL(url)
        baseUrl = u.protocol + "//" + u.host;

        const match = url.match(/\w+/g);    // return Arrayd or undefined

        if (match.includes("juejin")) {
            siteName = match[1] + "_" + match[3]
        } else if (match.includes("ithelp")) {
            siteName = match[1]
        } else if (match.includes("jianshu")) {
            siteName = match[2]
        } else if (config.re_pattern.zhihu_p.test(url)) {
            // for "https://zhuanlan.zhihu.com/p/29668000"
            siteName = "zhihu_p"
        } else if (config.re_pattern.zhihu_zhuanlan.test(url)) {
            // for "https://zhuanlan.zhihu.com/wxyyxc1992"
            siteName = "zhihu_zhuanlan"
        } else if (config.re_pattern.zhihu_people.test(url)) {
            // for "https://www.zhihu.com/people/wxyyxc1992/posts"
            siteName = "zhihu_people"
        } else if (config.re_pattern.zhihu_topic.test(url)) {
            // for "https://www.zhihu.com/topic/20105060/hot"
            siteName = "zhihu_topic"
        } else if (match.includes("jb51")) {
            siteName = "jb51"
        } else if (match.includes("cnblogs")) {
            siteName = "cnblogs"
        } else if (match.includes("logdown")) {
            siteName = "temp"
        } else {
            throw new Error("URL not support")
        }

        return { baseUrl, siteName }
    }

    function getSeletor(siteName) {
        assert(siteName != undefined, "siteName not provide")

        let selector = css[siteName]

        if (selector == undefined) {
            throw new Error("get selector error ...")
        } else {
            return selector
        }
    }

    let { baseUrl, siteName } = getSiteName(url)
    selector = getSeletor(siteName)

    return {
        url,
        selector,
        siteName,
        baseUrl,
        getSiteName,
        getSeletor,
    }
}

module.exports = Site
