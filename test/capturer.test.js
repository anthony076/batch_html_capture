const fs = require("fs")
const { Handler, handle_noScrolDown, handle_scrolDown, outputRename } = require("../lib/capturer")
// 避免 puppeteer 的測試出現 timeout
jest.setTimeout(40000)

describe("test handle()", () => {
    const testCase = [
        { name: "ithelp", siteName: 'ithelp', expected: handle_noScrolDown },
        { name: "jianshu", siteName: 'jianshu', expected: handle_scrolDown },

        { name: "juejin_post", siteName: 'juejin_post', expected: handle_scrolDown },
        { name: "juejin_collection", siteName: 'juejin_collection', expected: handle_scrolDown },
        { name: "juejin_user", siteName: 'juejin_user', expected: handle_scrolDown },
        { name: "juejin_user", siteName: 'juejin_user', expected: handle_scrolDown },

        { name: "zhihu_p", siteName: 'zhihu_p', expected: handle_scrolDown },
        { name: "zhihu_people", siteName: 'zhihu_people', expected: handle_scrolDown },
        { name: "zhihu_zhuanlan", siteName: 'zhihu_zhuanlan', expected: handle_scrolDown },
        { name: "zhihu_topic", siteName: 'zhihu_topic', expected: handle_scrolDown },

    ]

    const testMacro = (siteName, expected) => {
        expect(Handler(siteName)).toEqual(expected);
    };

    for (var condition of testCase) {
        test(condition.name, testMacro.bind(this, condition.siteName, condition.expected));
    }
})

describe("test handle_noScrolDown()", async () => {
    let linker = {
        links: ['https://ithelp.ithome.com.tw/articles/10184853'],
        selector:
        {
            name: 'ithelp',
            title_el: 'body > div.container.index-top > div > div > div.board.leftside.profile-main > div.ir-profile-content> div.ir-profile-series > h3',
            link_el: '.qa-list__title-link',
            paginate_el: '.profile-pagination > ul > li',
            article_el: '.qa-header__title',
            capture_el: 'body > div.container.index-top > div > div > div.leftside > div.qa-panel.clearfix'
        },
        folderName: '從0到100打造一個React Native boilerplate 系列'
    }

    if (!fs.existsSync(linker.folderName)) {
        fs.mkdirSync(linker.folderName);
    }

    test("check handle_noScrolDown() output file exist", async () => {
        await handle_noScrolDown(linker)
        expect(fs.readdirSync(`./${linker.folderName}`)[0].split(".")[1]).toBe("png")

    })

})

describe("test handle_scrolDown()", async () => {
    let linker = {
        links: ['https://zhuanlan.zhihu.com/p/29668000'],
        selector:
        {
            name: 'zhihu_p',
            title_el: null,
            link_el: null,
            paginate_el: null,
            article_el: 'main > div > article > header > h1.Post-Title',
            capture_el: 'main > div > article'
        },
        folderName: 'temp',
        delay: 1000
    }

    if (!fs.existsSync(linker.folderName)) {
        fs.mkdirSync(linker.folderName);
    }

    test("check handle_scrolDown() output file exist", async () => {
        await handle_scrolDown(linker)
        expect(fs.readdirSync(`./${linker.folderName}`)[0].split(".")[1]).toBe("png")
    })

})

// TODO，EBUSY error: resource busy or locked for testing capturer.outputRename()
// manually test this section temporarily
describe.skip("test outputRename()", async () => {
    test("check output file name", async () => {
        // mock function
        let capturer = require("../lib/capturer")
        capturer.filterString = jest.fn().mockReturnValue("ttt")

        // create a mock png
        await fs.copyFile('./mock/screen-1.png', './mock/mock.png', (err) => {
            if (err) throw err;
            console.log('mock.png is created');
        });

        // test function
        let css_tilte = "main > div > article > header > h1.Post-Title"
        let folderName = "temp"
        let outFileName = `./mock/mock.png`

        await capturer.outputRename(css_tilte, folderName, outFileName)

        // verify file existed
        fs.existsSync("./temp/ttt.png")
    })

})

