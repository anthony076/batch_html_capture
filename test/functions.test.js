
const { getParaType } = require("../lib/functions.js")

describe("test getParaType()", () => {
    const testCase = [
        { name: "-f para", argv: { _: [""], f: 'aa', file: 'aa', '$0': 'index.js' }, expected: { type: "file", para: "aa" } },
        { name: "-l para", argv: { _: [""], l: 'aa', list: 'aa', '$0': 'index.js' }, expected: { type: "list_url", para: "aa" } },
        { name: "single", argv: { _: ["aa"], '$0': 'index.js' }, expected: { type: "single", para: "aa" } },
    ]

    const testMacro = (argv, expected) => {
        expect(getParaType(argv)).toEqual(expected)
    };

    for (var condition of testCase) {
        test(condition.name, testMacro.bind(this, condition.argv, condition.expected));
    }

    test("command-line-error", () => {
        let argv = { _: [], '$0': 'index.js' }
        expect(() => {
            getParaType(argv)
        }).toThrowError("command-line parameter not available");
    })
})





