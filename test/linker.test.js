const cheerio = require('cheerio');
const { Linker, filterString, staticParser, scrollToBottom, getTitle, getPaginateNumber, getLinksByUrl } = require("../lib/linker")

describe("test filterString()", () => {
    it("general string", () => {
        const title = "Day1，Lession1: how are you/are your ok? ->"
        expect(filterString(title)).toBe("Day1，Lession1_how are you_are your ok _")
    })
})

describe("test staticParser()", () => {
    it("verify resolve", async () => {
        const site = {
            url: 'https://ithelp.ithome.com.tw/users/20103361/ironman/1043',
            selector: '.profile-pagination > ul > li',
            fn: getPaginateNumber
        }

        /* let pn = await staticParser(site)
        expect(pn).toBe("3") */
        return staticParser(site).then(
            (result) => {
                expect(result).toBe("3")
            }
        )
    })

    it("verify reject", async () => {
        const site = {
            url: 'http://gggyy/',
            selector: '.profile-pagination > ul > li',
            fn: getPaginateNumber
        }

        return expect(staticParser(site)).rejects.toBe("[Error] request error ...")
    })
})

describe("test getTitle()", () => {
    it("should return title", () => {
        const body = "<h1>i am tilte</h1>"
        const $ = cheerio.load(body)
        const selector = "h1"

        return expect(getTitle($, selector)).resolves.toBe("i am tilte")
    })

    it("cant find title", () => {
        const body = "<h1>i am tilte</h1>"
        const $ = cheerio.load(body)
        const selector = "h2"

        return expect(getTitle($, selector)).rejects.toBe("[Error] Cant find title ...")
    })
})

describe("test getPaginateNumber()", () => {
    it("should return pageNumber", () => {
        const body = `
        <ul>
            <li>1</li>
            <li>2</li>
            <li>3</li>
            <li>4</li>
            <li>5</li>
        </ul>
        `
        let $ = cheerio.load(body)
        return expect(getPaginateNumber($, "li")).resolves.toBe("4")
    })

    it("can't get pageNumber", () => {
        const body = `
        <ul>
            <li>1</li>
            <li>2</li>
            <li>3</li>
            <li>4</li>
            <li>5</li>
        </ul>
        `
        let $ = cheerio.load(body)
        return expect(getPaginateNumber($, "ll")).rejects.toBe("[Error] Cant get pageNumber ...")
    })
})

describe("test getLinksByUrl()", () => {

    it("should return link list", () => {
        const body = `
        <ul>
            <li><a href="http://aa.bb.cc.1">1</a></li>
            <li><a href="http://aa.bb.cc.2">2</a></li>
            <li><a href="http://aa.bb.cc.3">3</a></li>
            <li><a href="http://aa.bb.cc.4">4</a></li>
            <li><a href="http://aa.bb.cc.5">5</a></li>
        </ul>
        `
        const $ = cheerio.load(body)
        const expected = ["http://aa.bb.cc.1", "http://aa.bb.cc.2", "http://aa.bb.cc.3", "http://aa.bb.cc.4", "http://aa.bb.cc.5"]

        return expect(getLinksByUrl($, "li > a")).resolves.toEqual(expected)
    })

    it("cant get any link in page", () => {
        const body = `
        <ul>
            <li><a href="http://aa.bb.cc.1">1</a></li>
            <li><a href="http://aa.bb.cc.2">2</a></li>
            <li><a href="http://aa.bb.cc.3">3</a></li>
            <li><a href="http://aa.bb.cc.4">4</a></li>
            <li><a href="http://aa.bb.cc.5">5</a></li>
        </ul>
        `
        const $ = cheerio.load(body)

        return expect(getLinksByUrl($, "aa")).rejects.toEqual("[Error] cant get any link in page ...")
    })

})

describe.only("test _getLinksFromFile()", () => {

    it("should return link list", () => {

    })

})

/*
_getLinksFromSingle
Linker
describe.only("test aa()", () => {
    it("bb", () => {
    })
})
*/
