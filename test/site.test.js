const Site = require("../lib/site")
describe("test Site()", () => {

    const testCase = [
        { name: "ithelp", url: "https://ithelp.ithome.com.tw/users/20107244/ironman/1726", expected: "ithelp" },
        { name: "ithelp", url: "https://ithelp.ithome.com.tw/users/20103361/ironman/1043", expected: "ithelp" },

        { name: "juejin_post", url: "https://juejin.im/post/5c6805146fb9a049fa104d18", expected: "juejin_post" },
        { name: "juejin_collection", url: "https://juejin.im/collection/5c88b02ae51d453f6715b38e", expected: "juejin_collection" },
        { name: "juejin_user", url: "https://juejin.im/user/5a6c041ef265da3e3e34019a/posts", expected: "juejin_user" },

        { name: "jianshu_single_post", url: "https://www.jianshu.com/p/698b44eaa900", expected: "jianshu" },
        { name: "jianshu_user_list", url: "https://www.jianshu.com/u/112c6a5df44d", expected: "jianshu" },
        { name: "jianshu_user_notebook", url: "https://www.jianshu.com/nb/10098148", expected: "jianshu" },
        { name: "jianshu_zhuanlan", url: "https://www.jianshu.com/c/69b548699816", expected: "jianshu" },

        { name: "zhihu_p", url: "https://zhuanlan.zhihu.com/p/29668000", expected: "zhihu_p" },
        { name: "zhihu_people", url: "https://www.zhihu.com/people/wxyyxc1992/posts", expected: "zhihu_people" },
        { name: "zhihu_zhuanlan", url: "https://zhuanlan.zhihu.com/c_1090909802070233088", expected: "zhihu_zhuanlan" },
        { name: "zhihu_topic", url: "https://www.zhihu.com/topic/20688196/hot", expected: "zhihu_topic" },
        { name: "jb51", url: "https://www.jb51.net/article/69151.htm", expected: "jb51" },
    ]

    const testMacro = (url, expected) => {
        expect(Site(url).selector.name).toEqual(expected);
    };

    for (var condition of testCase) {
        test(condition.name, testMacro.bind(this, condition.url, condition.expected));
    }

    test("verify Unknown siteName error", () => {
        const url = "http://www.google.com.tw"

        let verify_unknown_error = () => {
            Site(url)
        }

        expect(verify_unknown_error).toThrow("URL not support")
    })
})